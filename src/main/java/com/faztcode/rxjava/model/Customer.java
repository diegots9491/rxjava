package com.faztcode.rxjava.model;

import com.faztcode.rxjava.util.ConstantStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@Document(collection = "customer")
public class Customer implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  private String id;
  @Field(name = "first_name")
  private String firstName;
  @Field(name = "last_name")
  private String lastName;
  @Field(name = "status")
  private ConstantStatus status;
}
