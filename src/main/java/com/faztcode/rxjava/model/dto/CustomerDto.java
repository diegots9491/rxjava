package com.faztcode.rxjava.model.dto;

import com.faztcode.rxjava.util.ConstantStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class CustomerDto implements Serializable {

  private static final long serialVersionUID = 1L;
  private String id;
  private String firstName;
  private String lastName;
  private ConstantStatus status;
}
