package com.faztcode.rxjava.repository;

import com.faztcode.rxjava.model.Customer;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;


@Repository
public interface ICustomerRepository extends MongoRepository<Customer, String> {
  List<Customer> findByFirstName(String firstName, PageRequest pageRequest);
  Optional<Customer> findByLastName(String lastName);
}
