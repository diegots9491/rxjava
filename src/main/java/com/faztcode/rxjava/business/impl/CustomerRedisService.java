package com.faztcode.rxjava.business.impl;

import com.faztcode.rxjava.business.ICustomerRedisService;
import com.faztcode.rxjava.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("CustomerRedisService")
public class CustomerRedisService implements ICustomerRedisService {

  @Autowired
  private RedisTemplate<String, Customer> redisTemplate;

  public static final String KEY = "CUSTOMER";

  HashOperations<String, String, Customer> hashOperations;

  @PostConstruct
  private void init() {
    hashOperations = redisTemplate.opsForHash();
  }

  @Override
  public void saveRedisCustomer(Customer customer) {
    hashOperations.put(KEY, customer.getId(), customer);
  }
}
