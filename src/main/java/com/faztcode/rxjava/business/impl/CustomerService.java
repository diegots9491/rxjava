package com.faztcode.rxjava.business.impl;

import com.faztcode.rxjava.business.ICustomerRedisService;
import com.faztcode.rxjava.business.ICustomerService;
import com.faztcode.rxjava.model.Customer;
import com.faztcode.rxjava.model.dto.CustomerDto;
import com.faztcode.rxjava.repository.ICustomerRepository;
import com.faztcode.rxjava.util.AppUtil;
import com.faztcode.rxjava.util.ConstantStatus;
import com.faztcode.rxjava.util.ErrorMessage;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service("CustomerService")
public class CustomerService implements ICustomerService {

  @Autowired
  private ICustomerRepository customerRepository;
  @Autowired
  @Qualifier("CustomerRedisService")
  private ICustomerRedisService customerRedisService;

  @Override
  @Transactional(readOnly = true)
  public Single<List<CustomerDto>> findAllCustomer() {
    Single<List<Customer>> listSingle = Single.create(singleEmitter -> {
      List<Customer> customerDtoList = customerRepository.findAll();
      singleEmitter.onSuccess(customerDtoList);
    });
    return listSingle.map(AppUtil::entityToCustomerList);
  }

  @Override
  @Transactional(readOnly = true)
  public Single<List<CustomerDto>> findCustomerByFirstName(String firstName) {
    Single<List<Customer>> listSingle = Single.create(singleEmitter -> {
      List<Customer> customers = customerRepository.findByFirstName(firstName, PageRequest.of(0, 4, Direction.ASC, "firstName"));
      singleEmitter.onSuccess(customers);
    });
    return listSingle.map(AppUtil::entityToCustomerList);
  }

  @Override
  @Transactional(readOnly = true)
  public Single<CustomerDto> findOneCustomer(String id) {
    Single<Customer> singleCustomer = Single.create(singleEmitter -> {
      Optional<Customer> customer = customerRepository.findById(id);
      if (customer.isEmpty()) {
        singleEmitter.onError(ErrorMessage.exceptionMessage("Customer not found"));
      } else {
        singleEmitter.onSuccess(customer.get());
      }
    });
    return singleCustomer.map(AppUtil::entityToCustomer);
  }

  @Override
  @Transactional
  public Single<CustomerDto> saveCustomer(CustomerDto customerDto) {
    Single<Customer> singleCustomer = Single.create(singleEmitter -> {
      Optional<Customer> customer = customerRepository.findByLastName(customerDto.getLastName());
      if (customer.isPresent()) {
        singleEmitter.onError(ErrorMessage.exceptionMessage("Customer Found"));
      } else {
        Customer newCustomer = AppUtil.customerDtoToEntity(customerDto);
        Customer saveCustomer = customerRepository.save(newCustomer);
        customerRedisService.saveRedisCustomer(saveCustomer);
        singleEmitter.onSuccess(saveCustomer);
      }
    });
    return singleCustomer.map(AppUtil::entityToCustomer);
  }

  @Override
  @Transactional
  public Single<CustomerDto> updateCustomer(CustomerDto customerDto, String id) {
    Single<Customer> singleCustomer = Single.create(singleEmitter -> {
      Optional<Customer> customer = customerRepository.findById(id);
      if (customer.isEmpty()) {
        singleEmitter.onError(ErrorMessage.exceptionMessage("Customer Found"));
      } else {
        Customer newCustomer = AppUtil.customerDtoToEntity(customerDto);
        singleEmitter.onSuccess(customerRepository.save(newCustomer));
      }
    });
    return singleCustomer.map(AppUtil::entityToCustomer);
  }

  @Override
  @Transactional
  public Single<CustomerDto> deleteCustomer(String id) {
    Single<Customer> singleCustomer = Single.create(singleEmitter -> {
      Optional<Customer> customer = customerRepository.findById(id);
      if (customer.isEmpty()) {
        singleEmitter.onError(ErrorMessage.exceptionMessage("Customer Found"));
      } else {
        customer.get().setStatus(ConstantStatus.INACTIVE);
        singleEmitter.onSuccess(customerRepository.save(customer.get()));
      }
    });
    return singleCustomer.map(AppUtil::entityToCustomer);
  }
}
