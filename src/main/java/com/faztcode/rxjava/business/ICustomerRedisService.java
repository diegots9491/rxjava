package com.faztcode.rxjava.business;

import com.faztcode.rxjava.model.Customer;

public interface ICustomerRedisService {
  void saveRedisCustomer(Customer customer);
}
