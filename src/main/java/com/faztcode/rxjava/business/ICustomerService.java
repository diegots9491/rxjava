package com.faztcode.rxjava.business;

import com.faztcode.rxjava.model.dto.CustomerDto;
import io.reactivex.Single;
import java.util.List;

public interface ICustomerService {
  Single<List<CustomerDto>> findAllCustomer();

  Single<List<CustomerDto>> findCustomerByFirstName(String firstName);

  Single<CustomerDto> findOneCustomer(String id);

  Single<CustomerDto> saveCustomer(CustomerDto customerDto);

  Single<CustomerDto> updateCustomer(CustomerDto customerDto, String id);

  Single<CustomerDto> deleteCustomer(String id);
}
