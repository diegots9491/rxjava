package com.faztcode.rxjava.util;

import com.faztcode.rxjava.model.Customer;
import com.faztcode.rxjava.model.dto.CustomerDto;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class AppUtil {

  private AppUtil(){}

  public static Customer customerDtoToEntity(CustomerDto customerDto) {
    Customer customer = new Customer();
    BeanUtils.copyProperties(customerDto, customer);
    return customer;
  }

  public static CustomerDto entityToCustomer(Customer customer) {
    CustomerDto customerDto = new CustomerDto();
    BeanUtils.copyProperties(customer, customerDto);
    return customerDto;
  }
  public static List<CustomerDto> entityToCustomerList(List<Customer> customer) {
    return customer.stream()
      .map(AppUtil::entityToCustomer)
      .collect(Collectors.toList());
  }
}
