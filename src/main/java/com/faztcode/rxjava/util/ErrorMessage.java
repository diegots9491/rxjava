package com.faztcode.rxjava.util;

public class ErrorMessage {

  private ErrorMessage(){}

  public static Throwable exceptionMessage(String error) {
    return new Throwable(error);
  }
}
