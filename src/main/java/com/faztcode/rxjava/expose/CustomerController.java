package com.faztcode.rxjava.expose;

import com.faztcode.rxjava.business.ICustomerService;
import com.faztcode.rxjava.model.dto.CustomerDto;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
  @Autowired
  @Qualifier("CustomerService")
  private ICustomerService customerService;

  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<List<CustomerDto>>> findAllCustomer() {
    return customerService.findAllCustomer()
      .subscribeOn(Schedulers.io())
      .map(customerDos -> ResponseEntity.ok().body(customerDos));
  }

  @GetMapping(value = "/firstname/{firstname}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<List<CustomerDto>>> findByFirstName(@PathVariable("firstname") String firstname) {
    return customerService.findCustomerByFirstName(firstname)
      .subscribeOn(Schedulers.io())
      .map(customerDos -> ResponseEntity.ok().body(customerDos));
  }

  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<CustomerDto>> findOneCustomer(@PathVariable("id") String id) {
    return customerService.findOneCustomer(id)
      .subscribeOn(Schedulers.io())
      .map(customerDos -> ResponseEntity.ok().body(customerDos));
  }

  @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<CustomerDto>> saveCustomer(@RequestBody CustomerDto customerDto) {
    return customerService.saveCustomer(customerDto)
      .subscribeOn(Schedulers.io())
      .map(customerDos -> ResponseEntity.ok().body(customerDos));
  }

  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<CustomerDto>> updateCustomer(@PathVariable("id") String id, @RequestBody CustomerDto customerDto) {
    return customerService.updateCustomer(customerDto, id)
      .subscribeOn(Schedulers.io())
      .map(customerDos -> ResponseEntity.ok().body(customerDos));
  }

  @DeleteMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<CustomerDto>> deleteCustomer(@PathVariable("id") String id) {
    return customerService.deleteCustomer(id)
      .subscribeOn(Schedulers.io())
      .map(customerDos -> ResponseEntity.ok().body(customerDos));
  }
}
